import pyspark
from pyspark.sql import SQLContext, SparkSession
from pyspark import SparkContext
import pandas as pd
from pandas import DataFrame


sc = SparkContext()
sqlContext = SQLContext(sc)

train_data_frame = sqlContext.read.parquet('/home/vlado/Desktop/power_line_fault_det/train.parquet')

print(type(train_data_frame))

result = train_data_frame.select('*').toPandas()

print(result)

#
# df2 = train_data_frame.select(train_data_frame.col1,train_data_frame.col2)
# df2.coalesce(1).write.format('json').save('/home/vlado/Desktop/power_line_fault_det/file_name.json')



#train_data_frame.show()
# b = train_data_frame.describe().show()
#
# print(b.head(10))
#
# print (type(train_data_frame))
#
# pandas_df = train_data_frame.toPandas()
#
# print (type(pandas_df))
